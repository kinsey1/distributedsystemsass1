package activitystreamer.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import activitystreamer.server.OutgoingMessage;
import netscape.javascript.JSObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import activitystreamer.util.Settings;

public class ClientSkeleton extends Thread {
    private static final Logger log = LogManager.getLogger();
    private static ClientSkeleton clientSolution;
    private TextFrame textFrame;

    static Socket socket;
    static BufferedReader in;
    static BufferedWriter out;

    private String username;
    private String secret;
    private boolean loggedIn;

    private  boolean autoLogin;
    private boolean disconnect = false;

    public static ClientSkeleton getInstance() {
        if (clientSolution == null) {
            clientSolution = new ClientSkeleton();
        }
        return clientSolution;
    }

    public ClientSkeleton() {
        loggedIn = false;
        textFrame = new TextFrame();
        this.username = "";
        this.secret = "";

        socket = null;
        try {
            connect(Settings.getRemoteHostname(), Settings.getRemotePort());
        } catch (UnknownHostException e) {
            System.out.println("Socket:" + e.getMessage());
        } catch (EOFException e) {
            System.out.println("EOF:" + e.getMessage());
        } catch (IOException e) {
            System.out.println("readlline:" + e.getMessage());
        } catch (NullPointerException e) {
            log.error("Please start the server first");
        }
        if (this.in!=null){
            this.start();
        }else{
            log.error("Please start server first");
        }

    }

    /**
     * Connects to a remote port and sets the socket, buffered reader and
     * writer
     *
     * @param remoteHost
     * @param remotePort
     * @throws IOException
     */
    public void connect(String remoteHost, int remotePort) throws IOException {
        this.socket = new Socket(remoteHost, remotePort);
        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
        this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));
    }

    /**
     * Sends a message from the client to the server
     *
     * @param jsonMessage
     * @throws ParseException
     */
    public void sendMessage(JSONObject jsonMessage) {
        log.debug(jsonMessage.toString());
        parseOutgoingMessage(jsonMessage);

    }

    public void flushMessage(JSONObject jsonMessage){
        String jsonStr = jsonMessage.toString();
        try {
            // Client sends message
            out.write(jsonStr + "\n");

            out.flush();
            log.info("Message sent " + jsonStr);

        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Function only used for Login messages. Could be extended to cover more
     *
     * @param message
     */
    public void parseOutgoingMessage(JSONObject message) {
        //TODO check if login with username and without secret (By generating secret and sending register)
        //TODO I know this is a bit much but it should work
        // We could could also make this parse invalid json messages
        if (message.containsKey("command")
                && (message.get("command").equals("LOGIN")
                || message.get("command").equals("REGISTER")
                || message.get("command").equals("LOGOUT"))) {
            if (message.get("command").equals("LOGIN")) {
                if (!message.containsKey("username")) {
                    flushMessage(message);
                    return;
                } else if ( (message.containsKey("username")
                        && message.containsKey("secret")) || (message.get("username").equals("anonymous"))) {
                    this.username = message.get("username").toString();
                    if (message.containsKey("secret")) {
                        this.secret = message.get("secret").toString();
                    }
                    flushMessage(message);
                    return;
                } else if (message.containsKey("username")) {
                    this.secret = Settings.generateID();
                    this.username = message.get("username").toString();
                    this.autoLogin = true;
                    log.info("Creating secret");
                    OutgoingMessage msg = new OutgoingMessage("REGISTER", this.username, this.secret);
                    log.info(msg.getReply());
                    flushMessage(msg.getReply());
                    return;
                }

            } else if (message.get("command").equals("LOGOUT")) {
                flushMessage(message);
                try {
                    disconnect();
                } catch (Exception e) {
                    log.info("Couldn't disconnect");
                }
            } else if (message.get("command").equals("REGISTER"))
                flushMessage(message);
        }
        else if (message.containsKey("command") && message.get("command").equals("ACTIVITY_MESSAGE")) {
            flushMessage(message);
        }
        else {
            String sec = "";
            if(this.username.equals("anonymus"))
                sec = "null";
            else
                sec = this.secret;
            OutgoingMessage outgoing = new OutgoingMessage("ACTIVITY_MESSAGE",this.username,
                    sec,message);
            flushMessage(outgoing.getReply());
        }

    }


    public void disconnect() throws IOException {
        log.info("closing the socket");
        this.disconnect= true;
        textFrame.setOutputText(new JSONObject());
        socket.close();


    }

    /**
     * Sends the login message used for redirecting
     */
    public void sendLoginMessage() {
        OutgoingMessage message = new OutgoingMessage("LOGIN", this.username, this.secret);
        String jsonStr = message.getJsonReply();

        try {

            out.write(jsonStr + "\n");
            out.flush();
            log.info("Sending redirected login message");
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Function which checks the incoming message. If its a login,
     * we set the client to logged in. If its a redirect then we reconnect
     * to a separate server.
     *
     * @param message
     */
    public void processIncomingMessage(JSONObject message) {
        if (message.containsKey("command")) {
            if (message.get("command").equals("LOGIN_SUCCESS")) {
                this.loggedIn = true;
            }
            if(message.get("command").equals("REGISTER_SUCCESS") && autoLogin){
                OutgoingMessage outgoingMessage= new OutgoingMessage("LOGIN", this.username, this.secret);
                log.info("Secret for further user is : " +this.secret);
                flushMessage(outgoingMessage.getReply());
            }
            if (loggedIn &&
                    message.get("command").equals("REDIRECT")
                    && message.containsKey("hostname")
                    && message.containsKey("port")) {
                try {
                    log.info("Server requested redirect. Closing current connection and resending login");
                    this.socket.close();
                    this.loggedIn = false;
                    Settings.setRemoteHostname(message.get("hostname").toString());
                    Settings.setRemotePort(Integer.parseInt(message.get("port").toString()));
                    connect(Settings.getRemoteHostname(), Settings.getRemotePort());

                    sendLoginMessage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Overwritten function which is called using .start().
     * This function runs in a separate thread which is used
     * for reading the incoming messages.
     */
    public void run() {
        String received;
        try {
            while ( (!disconnect) && ((received = in.readLine()) != null)) {
                log.debug(received);
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(received);
                processIncomingMessage(json);
                textFrame.setOutputText(json);

                if (received != null) {
                    log.info("Response received");
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();

        }
    }
}
