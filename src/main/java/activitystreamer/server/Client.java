package activitystreamer.server;

/**
 * Client class to hold simple client information
 */
public class Client {
    private String username;
    private String secret;

    public Client(String username, String secret) {
        this.username = username;
        this.secret = secret;
    }

    public String getUsername() {
        return username;
    }

    public String getSecret() {
        return secret;
    }
}
