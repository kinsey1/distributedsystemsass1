package activitystreamer.server;

import activitystreamer.util.Settings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Class which handles controlling all connections
 */
public class Control extends Thread {

    private static final Logger log = LogManager.getLogger();
    protected static Control control = null;
    private static ArrayList<Connection> connections;
    private static ArrayList<Client> clients;
    private static HashMap<Connection, ServerInfo> serverConnections;
    private static HashMap<Connection, Client> clientConnections;
    private static HashMap<String, RegisterLock> registerLocks;
    private static boolean term = false;
    private static Listener listener;
    private static Connection remoteServer;
    private static String ID;
    private Client anon;

    public Control() {
        // Initialize maps which map connections to clients / servers
        this.ID = Settings.generateID();
        log.info("SERVER ID : " + this.ID);
        connections = new ArrayList<Connection>();
        clientConnections = new HashMap<Connection, Client>();
        serverConnections = new HashMap<Connection, ServerInfo>();
        registerLocks = new HashMap<String, RegisterLock>();

        //Initiate and add a client to the clients array
        clients = new ArrayList<Client>();

        //Anonymous client
        anon = new Client("anonymous", "null");
        clients.add(anon);

        //Initiate an outgoing connection
        initiateConnection();
        try {
            listener = new Listener();
        } catch (IOException e1) {
            log.fatal("failed to startup a listening thread: " + e1);
            System.exit(-1);
        }
        this.start();
    }

    public static Control getInstance() {
        if (control == null) {
            control = new Control();
        }
        return control;
    }

    /**
     * Create a remote connection to another server upon startup
     */
    public void initiateConnection() {
        log.info("Attempt connecting to : rp:" + Settings.getRemotePort() + " rh : " + Settings.getRemoteHostname());
        if (Settings.getRemoteHostname() != null) {
            try {

                Socket socket = new Socket(Settings.getRemoteHostname(), Settings.getRemotePort());
                remoteServer = outgoingConnection(socket);

                OutgoingMessage authMsg = new OutgoingMessage("AUTHENTICATE", Settings.getSecret());
                log.info("Sending auth message: " + authMsg.getJsonReply());
                remoteServer.writeMsg(authMsg.getJsonReply());

                //Sets it as an authenticated server until a message or no message is received (good)
                connections.add(remoteServer);
                remoteServer.setAuthenticate(true);
                ServerInfo authServer = new ServerInfo(Settings.getRemoteHostname(), Settings.getRemotePort());
                this.serverConnections.put(remoteServer, authServer);
            } catch (IOException e) {
                log.error("failed to make connection to " + Settings.getRemoteHostname() + ":" + Settings.getRemotePort() + " :" + e);
                //@TODO Do we want to exit here?
            }
        } else
            log.info("First Server in the system. The secret is " + Settings.getSecret());
    }

    /**
     * Authenticates an incoming server connection.
     *
     * @param con
     * @param secret
     * @return
     * @throws IOException
     */
    private boolean authenticateServer(Connection con, String secret) {
        //check if secret is correct and its not already an authenticated server
        if (secret.equals(Settings.getSecret())) {
            con.setAuthenticate(true);
            con.setServer(true);
            ServerInfo authServer = new ServerInfo();
            this.serverConnections.put(con, authServer);
            return true;
        }
        return false;

    }

    /**
     * Processes incoming server announce. Updates the serverInfo object
     * which is mapped to this connection.
     *
     * @param msg
     * @param con
     * @return
     */
    private boolean processAnnounce(IncomingMessage msg, Connection con) {
        if (con.isAuthenticated() && serverConnections.containsKey(con)) {
            String hostname = msg.jsonMessage.get("hostname").toString();
            String ID = msg.jsonMessage.get("id").toString();

            int port = Integer.parseInt(msg.jsonMessage.get("port").toString());
            int load = Integer.parseInt(msg.jsonMessage.get("load").toString());

            ServerInfo announcingServer = serverConnections.get(con);
            announcingServer.setHostname(hostname);
            announcingServer.setID(ID);
            announcingServer.setLoad(load);
            announcingServer.setPort(port);
            announcingServer.setValid(true);
            return true;
        }
        return false;
    }

    /**
     * Processes an incoming login message and
     * returns the relevant OutgoingMessge type.
     *
     * @param msg
     * @param con
     * @return
     */
    private OutgoingMessage processLogin(IncomingMessage msg, Connection con) {
        String username, secret;
        // The following block ensures that a username and secret is sent by the client
        try {
            //Anonymous login
            username = msg.jsonMessage.get("username").toString();
            if (username.equals("anonymous")) {
                clientConnections.put(con, anon);
                con.setServer(false);
                return new OutgoingMessage("LOGIN_SUCCESS",
                        " Unauthenticated login as : " + username);
            } else {
                secret = msg.jsonMessage.get("secret").toString();
            }
        } catch (Exception e) {
            return new OutgoingMessage("INVALID_MESSAGE",
                    "No secret provided");
        }
        for (Client cli : clients) {
            if (cli.getUsername().equals(username)) {
                if (cli.getSecret().equals(secret)) {
                    clientConnections.put(con, cli);
                    return new OutgoingMessage("LOGIN_SUCCESS",
                            "logged in as user " + username);
                } else {
                    return new OutgoingMessage("LOGIN_FAILED",
                            "incorrect secret - " + secret
                                    + " provided for user " + username);
                }
            }
        }
        return new OutgoingMessage("LOGIN_FAILED",
                "no registered user with username - " + username);
    }

    /**
     * Registers a new client, takes an incoming message
     * which has the type of register. Broadcasts the request
     * to other servers as a LOCK_REQUEST.
     * The user is not registered until LOCK_ALLOWED has come back
     * from every server
     *
     * @param incoming
     * @return
     */
    private boolean registerClient(IncomingMessage incoming, Connection con) {
        String username = incoming.jsonMessage.get("username").toString();
        String secret = incoming.jsonMessage.get("secret").toString();

        Boolean available = true;

        for (Client curClient : clients) {
            if (curClient.getUsername().equals(username)) {
                available = false;
            }
        }
        // If its locally available send out lock request
        if (available) {
            OutgoingMessage lockMessage = new OutgoingMessage("LOCK_REQUEST", username, secret);
            log.debug("BROADCASTING " + lockMessage.getJsonReply());
            broadcastActivity(lockMessage, false, true);
            registerLocks.put(username, new RegisterLock(new Client(username, secret), serverConnections.size(), con));
            return true;
        }
        return false;
    }

    /**
     * Checks the registered users if a user is already registered
     * -> used for lock requests
     *
     * @param incoming
     * @return
     */
    private boolean checkRegisteredUsers(IncomingMessage incoming) {
        String username = incoming.jsonMessage.get("username").toString();
        for (Client client : clients) {
            if (username.equals(client.getUsername())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Function to broadcast a message to connected clients and servers
     *
     * @param message
     * @param client  to indicate whether this message came from a client
     *                or was broadcast from other servers.
     * @param server
     */
    public void broadcastActivity(OutgoingMessage message, boolean client, boolean server) {

        try {
            if (client) {
                for (Connection con : clientConnections.keySet()) {
                    log.error("broadcast to clients" + message.getJsonReply());
                    con.writeMsg(message.getJsonReply());
                }
            }
            if (server) {
                for (Connection con : serverConnections.keySet()) {

                    log.info("broadcast to servers" + message.getJsonReply());
                    con.writeMsg(message.getJsonReply());
                }
            }
        } catch (IOException e) {
            log.error("ERROR BROADCASTING");
        }
    }

    /**
     * Main process function. This method checks the type of message by using
     * the IncomingMessage class. Depending on type, it delegates work and
     * decides whether to respond, close or maintain the connection
     *
     * @param con
     * @param msg
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public synchronized boolean process(Connection con, String msg) throws IOException, ParseException {
        //TODO we need to work out if we want to throw and exception or
        //TODO or try catch each writeMsg()
        if (!term) {//in connection

            IncomingMessage incoming = new IncomingMessage(msg, con);

            if (incoming.type == IncomingMessage.Type.AUTHENTICATE) {
                if (con.isAuthenticated()) {
                    con.writeMsg(new OutgoingMessage("INVALID_MESSAGE",
                            "server already Authenticated").getJsonReply());
                    removeConnections(con);
                    return true;
                }
                String secret = incoming.jsonMessage.get("secret").toString();
                if (authenticateServer(con, secret)) {
                    log.info("Authenticated server with secret : " + secret);
                    return false;
                }
                con.writeMsg(new OutgoingMessage("AUTHENTICATION_FAIL", secret).getJsonReply());
                log.info(con + "failed to authenticate with : " + secret + " actual : " + Settings.getSecret());
                removeConnections(con);
                return true;
            } else if (incoming.type == IncomingMessage.Type.LOGIN) {
                if (con.isAuthenticated()) {
                    OutgoingMessage message = new OutgoingMessage("INVALID_MESSAGE", "Already logged in. ");
                    con.writeMsg(message.getJsonReply());
                    removeConnections(con);
                    return true;
                }
                OutgoingMessage validLoginmsg = processLogin(incoming, con);

                if (validLoginmsg.reply.get("command").equals("LOGIN_SUCCESS")) {

                    for (Connection servCon : serverConnections.keySet()) {
                        ServerInfo serv = serverConnections.get(servCon);
                        //If our server has 2 more connected clients
                        //send it a redirect
                        if (clientConnections.size() >= 3 + serv.getLoad() && serv.isValid()) {
                            con.writeMsg(validLoginmsg.getJsonReply());
                            con.writeMsg(new OutgoingMessage("REDIRECT",
                                    serv.getHostname(), serv.getPort().toString()).getJsonReply());
                            removeConnections(con);
                            return true;
                        }
                    }
                    con.setAuthenticate(true);
                    con.writeMsg(validLoginmsg.getJsonReply());
                    return false;
                } else {
                    //send message back to client accordingly
                    con.writeMsg(validLoginmsg.getJsonReply());
                    removeConnections(con);
                    return true;
                }
            } else if (incoming.type == IncomingMessage.Type.LOGOUT) {
                removeConnections(con);
                return true;
            } else if (incoming.type == IncomingMessage.Type.INVALID) {
                OutgoingMessage message = new OutgoingMessage("INVALID_MESSAGE", incoming.invalidString);
                con.writeMsg(message.getJsonReply());
                removeConnections(con);
                return true;

            } else if (incoming.type == IncomingMessage.Type.AUTHENTICATION_FAIL) {
                log.info("Tried to connect.. but failed");
                removeConnections(con);
                return true;
            } else if (incoming.type == IncomingMessage.Type.REGISTER) {
                String username = incoming.jsonMessage.get("username").toString();
                if (registerClient(incoming, con)) {
                    log.debug("INCOMING REGISTER : " + incoming.jsonMessage.toJSONString());

                    //If this is the first server
                    if (registerLocks.get(username).status()) {
                        OutgoingMessage message = new OutgoingMessage("REGISTER_SUCCESS", username + " registered successfully");
                        con.writeMsg(message.getJsonReply());
                    }
                    return false;
                } else {
                    con.writeMsg(new OutgoingMessage("REGISTER_FAILED", "").getJsonReply());
                    removeConnections(con);
                    return true;
                }
            } else if (incoming.type == IncomingMessage.Type.SERVER_ANNOUNCE) {
                log.info("Received server announce : ");
                log.info(con.getSocket().getRemoteSocketAddress());

                if (processAnnounce(incoming, con)) {
                    return false;
                } else {
                    con.writeMsg(new OutgoingMessage("INVALID_MESSAGE",
                            "Invalid server announce").getJsonReply());
                    removeConnections(con);
                    return true;
                }

            } else if (incoming.type == IncomingMessage.Type.ACTIVITY_MESSAGE) {
                JSONObject activity = (JSONObject) incoming.jsonMessage.get("activity");
                if (con.isAuthenticated()) {
                    Client cli = clientConnections.get(con);
                    if (((cli.getUsername().equals(incoming.jsonMessage.get("username")))
                            && (cli.getSecret().equals(incoming.jsonMessage.get("secret"))))
                            || incoming.jsonMessage.get("username").equals("anonymous")) {
                        String username = clientConnections.get(con).getUsername();

                        activity.put("authenticated_user", username);

                        OutgoingMessage activityBroadcastMessage =
                                new OutgoingMessage("ACTIVITY_BROADCAST",
                                        activity);

                        broadcastActivity(activityBroadcastMessage, true, true);
                        return false;

                    } else {
                        OutgoingMessage outgoingMessage = new OutgoingMessage(
                                "AUTHENTICATION_FAIL", "Incorrect secret or username provided"
                        );
                        removeConnections(con);
                        con.writeMsg(outgoingMessage.getJsonReply());
                        return true;
                    }
                } else {
                    OutgoingMessage outgoingMessage = new OutgoingMessage(
                            "AUTHENTICATION_FAIL", "Not Logged In"
                    );
                    removeConnections(con);
                    con.writeMsg(outgoingMessage.getJsonReply());
                    return true;
                }

            } else if (incoming.type == IncomingMessage.Type.ACTIVITY_BROADCAST) {
                if (con.isAuthenticated()) {
                    JSONObject activity = (JSONObject) incoming.jsonMessage.get("activity");
                    OutgoingMessage activityBroadCastMessage =
                            new OutgoingMessage("ACTIVITY_BROADCAST",
                                    activity);
                    broadcastActivity(activityBroadCastMessage, true, false);
                    return false;
                } else {
                    new OutgoingMessage("INVALID_MESSAGE", "Unauthenticated Server");
                    removeConnections(con);
                    return true;
                }
            } else if (incoming.type == IncomingMessage.Type.LOCK_REQUEST) {
                String username = incoming.jsonMessage.get("username").toString();
                String secret = incoming.jsonMessage.get("secret").toString();

                if (checkRegisteredUsers(incoming)) {
                    clients.add(new Client(username, secret));
                    broadcastActivity(
                            new OutgoingMessage("LOCK_ALLOWED", username, secret),
                            false, true);
                } else {
                    broadcastActivity(
                            new OutgoingMessage("LOCK_DENIED", username, secret),
                            false, true);
                }

            } else if (incoming.type == IncomingMessage.Type.LOCK_DENIED) {
                String username = incoming.jsonMessage.get("username").toString();
                if (registerLocks.containsKey(username)) {
                    registerLocks.remove(username);
                    OutgoingMessage message = new OutgoingMessage("REGISTER_FAILED", username + " is already registered");
                    con.writeMsg(message.getJsonReply());
                    removeConnections(con);
                    return true;
                }

                Iterator<Client> clientsIt = clients.iterator();
                while (clientsIt.hasNext()) {
                    if (clientsIt.next().getUsername().equals(username)) {
                        clientsIt.remove();
                    }
                }
                return false;

            } else if (incoming.type == IncomingMessage.Type.LOCK_ALLOWED) {
                String username = incoming.jsonMessage.get("username").toString();
                if (registerLocks.containsKey(username)) {
                    RegisterLock lock = registerLocks.get(username);
                    lock.addAllowed();
                    if (lock.status()) {
                        clients.add(lock.getClient());
                        OutgoingMessage message = new OutgoingMessage("REGISTER_SUCCESS", username + " registered successfully");
                        lock.getCon().writeMsg(message.getJsonReply());
                    }
                }

            }
            return false;
        }
        return false;

    }


    /*
     * The connection has been closed by the other party.
     */
    public synchronized void connectionClosed(Connection con) {
        if (!term) connections.remove(con);
    }

    /**
     * Creates a new connection type for incoming socket connections
     *
     * @param s
     * @return
     * @throws IOException
     */
    public synchronized Connection incomingConnection(Socket s) throws IOException {

        log.debug("incoming connection: " + Settings.socketAddress(s));
        Connection c = new Connection(s);
        connections.add(c);
        return c;

    }


    /**
     * A new outgoing connection has been established, and a reference is returned to it
     *
     * @param s
     * @return
     * @throws IOException
     */
    public synchronized Connection outgoingConnection(Socket s) throws IOException {
        log.debug("outgoing connection: " + Settings.socketAddress(s));
        Connection c = new Connection(s);
        connections.add(c);
        return c;

    }

    /**
     * Overridden run method to allow the broadcasting to happen in a separate
     * thread to the incoming message processing.
     */
    @Override
    public void run() {
        log.info("using activity interval of " + Settings.getActivityInterval() + " milliseconds");
        while (!term) {
            // do something with 5 second intervals in between
            try {
                Thread.sleep(Settings.getActivityInterval());
            } catch (InterruptedException e) {
                log.info("Received an interrupt, system is shutting down");
                break;
            }
            if (!term) {

                log.info("Current server list: ");
                for (Connection servConnection : serverConnections.keySet()) {
                    log.info("Server : " + serverConnections.get(servConnection).getHostname() +
                            " load: " + serverConnections.get(servConnection).getLoad());
                    //if (!servConnection.isAlive())
                    //    serverConnections.remove(servConnection);
                }
                log.info("Current client list: ");
                for (Connection clientConnection : clientConnections.keySet()) {
                    log.info(clientConnections.get(clientConnection).getUsername());
                    if(!clientConnection.isAlive())
                        clientConnections.remove(clientConnection);
                }
                term = broadcastServerAnnounce();

            }

        }
        log.info("closing " + connections.size() + " connections");
        // clean up
        for (Connection connection : connections) {
            connection.closeCon();
        }
        listener.setTerm(true);
    }

    /**
     * Activity function for announcing servers
     *
     * @return
     */
    private boolean broadcastServerAnnounce() {

        //broadcast
        for (Connection connection : serverConnections.keySet()) {
            String msg = new OutgoingMessage("SERVER_ANNOUNCE",
                    this.ID, clientConnections.size() + "",
                    Settings.getLocalHostname(),
                    Settings.getLocalPort() + "").getJsonReply();
            try {
                connection.writeMsg(msg);
            } catch (IOException e) {
                e.printStackTrace();
                log.info("Error writing broadcast message");
            }

        }

        return false;
    }

    public final void setTerm(boolean t) {
        term = t;
    }

    public final ArrayList<Connection> getConnections() {
        return connections;
    }

    private void removeConnections(Connection con) {
        if (clientConnections.containsKey(con))
            clientConnections.remove(con);
        if (serverConnections.containsKey(con))
            serverConnections.remove(con);
    }
}
