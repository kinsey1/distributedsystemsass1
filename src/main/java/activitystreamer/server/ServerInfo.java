package activitystreamer.server;


/**
 * Class to maintain the information about a connected server so that
 * it can be updated during server announces.
 */
public class ServerInfo {

    private String hostname;
    private Integer port;
    private Integer load;
    private String ID;
    private boolean valid = false;

    public ServerInfo(String username, Integer port) {
        this.hostname = username;
        this.port = port;
        this.ID = "";
        this.load = 0;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public ServerInfo() {
        this.load = 0;

    }

    public String getHostname() {
        return hostname;
    }

    public Integer getPort() {
        return port;
    }

    public void setLoad(Integer load) {
        this.load = load;
    }

    public String getID() {
        return this.ID;
    }

    public boolean isValid() {
        return this.valid;
    }

    public Integer getLoad() {
        return load;
    }

    public void setSecret(String secret) {
        this.ID = ID;
    }

    public String toString() {
        return this.hostname + " " + this.port + " " + this.load;
    }
}
