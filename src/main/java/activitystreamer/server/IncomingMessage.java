package activitystreamer.server;

import activitystreamer.server.Connection;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Class to process incoming messages,
 * generates a type for each incoming message
 * and contains functions to validate,
 * process, and create response.
 * A message is given a type
 * which confirms its validity
 */

public class IncomingMessage {

    private static final Logger log = LogManager.getLogger();

    JSONObject jsonMessage;
    JSONObject activityMessage;

    public enum Type {
        REGISTER,
        AUTHENTICATE,
        LOGIN,
        LOGOUT,
        SERVER_ANNOUNCE,
        ACTIVITY_MESSAGE,
        ACTIVITY_BROADCAST,
        LOCK_REQUEST,
        INVALID,
        LOCK_ALLOWED,
        LOCK_DENIED,
        AUTHENTICATION_FAIL,
    }

    //String used to give the info on an invalid message
    String invalidString;

    //Incoming message type
    Type type;


    public IncomingMessage(String msg, Connection con) {
        JSONParser parser = new JSONParser();
        try {
            this.jsonMessage = (JSONObject) parser.parse(msg);
            validateMessage(jsonMessage, con);

        } catch (ParseException e) {
            e.printStackTrace();
            this.type = Type.INVALID;
            //TODO create outoging invalid message and return
        }

    }

    /**
     * Chooses which  enum type the incoming
     * message is. and assignts the enum type
     *
     * @param message
     */
    public void validateMessage(JSONObject message, Connection con) {

        if (message.containsKey("command")) {
            String command = message.get("command").toString();
            switch (command) {

                case "AUTHENTICATE":
                    if (message.containsKey("secret")) {
                        type = Type.AUTHENTICATE;
                    }
                    break;

                case "AUTHENTICATION_FAIL":
                    type = Type.AUTHENTICATION_FAIL;
                    break;

                case "LOGIN":
                    if (message.containsKey("username")) {
                        type = Type.LOGIN;
                    } else {
                        type = Type.INVALID;
                        this.invalidString = "No username provided for Login command";
                    }
                    break;
                case "LOGOUT":
                    type = Type.LOGOUT;
                    break;

                case "SERVER_ANNOUNCE":
                    if (!con.isAuthenticated()) {
                        type = Type.INVALID;
                        invalidString = "Server isn't authenticated";
                    } else {
                        if (message.containsKey("id")
                                && message.containsKey("load")
                                && message.containsKey("hostname")
                                && message.containsKey("port"))
                            type = Type.SERVER_ANNOUNCE;
                        else {
                            type = Type.INVALID;
                            invalidString = "Incorrect message received";
                        }
                    }
                    break;

                case "ACTIVITY_BROADCAST":
                    if (!con.isAuthenticated()) {
                        type = Type.INVALID;
                        invalidString = "Server isn't authenticated";
                    } else {
                        if (message.containsKey("activity") &&
                                isJsonValid(message.get("activity").toString())) {
                            type = Type.ACTIVITY_BROADCAST;
                        } else {
                            type = Type.INVALID;
                            invalidString = "Activity object not valid JSON";
                        }
                    }
                    break;

                case "ACTIVITY_MESSAGE":
                    if (message.containsKey("username")
                            && message.containsKey("secret")
                            && message.containsKey("activity")
                            && isJsonValid(message.get("activity").toString())) {

                        type = Type.ACTIVITY_MESSAGE;

                    } else {
                        type = Type.INVALID;
                        invalidString = "Invalid Activity_message";
                    }
                    break;

                case "REGISTER":
                    if (!con.isAuthenticated()) {
                        if (message.containsKey("username") && message.containsKey("secret")) {
                            type = Type.REGISTER;
                        } else {
                            type = Type.INVALID;
                            this.invalidString = "Register not correct";
                        }
                    } else {
                        type = Type.INVALID;
                        this.invalidString = "Already Logged in";
                    }
                    break;
                case "LOCK_REQUEST":
                    if (!con.isAuthenticated()) {
                        type = Type.INVALID;
                        invalidString = "Not authenticated";
                    }
                    if (message.containsKey("username")
                            && message.containsKey("secret")) {
                        type = Type.LOCK_REQUEST;
                    } else {
                        type = Type.INVALID;
                        invalidString = "Bad lock request";
                    }
                    break;
                case "LOCK_DENIED":
                    if (!con.isAuthenticated()) {
                        type = Type.INVALID;
                        invalidString = "Not authenticated";
                    }
                    if (message.containsKey("username")
                            && message.containsKey("secret")) {
                        type = Type.LOCK_DENIED;
                    } else {
                        type = Type.INVALID;
                        invalidString = "Bad lock request";
                    }
                case "LOCK_ALLOWED":
                    if (!con.isAuthenticated()) {
                        type = Type.INVALID;
                        invalidString = "Not authenticated";
                    }
                    if (message.containsKey("username")
                            && message.containsKey("secret")) {
                        type = Type.LOCK_ALLOWED;
                    } else {
                        type = Type.INVALID;
                        invalidString = "Bad lock request";
                    }
            }

        }
    }

    /**
     * Checks if the incoming string is valid json, used for parsing activity
     * objects
     *
     * @param input
     * @return
     */
    private static boolean isJsonValid(String input) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject activity = (JSONObject) parser.parse(input);
            return true;
        } catch (ParseException e) {
            return false;
        }

    }

}
