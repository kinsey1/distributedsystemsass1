package activitystreamer.server;

import org.json.simple.JSONObject;

/**
 * Class to generate JSON outgoing messages.
 * Has multiple overloaded constructors to create
 * outgoing messages based on the number of input strings
 */
public class OutgoingMessage {

    public JSONObject reply;

    /**
     * Used for most messages
     *
     * @param command
     * @param message
     */
    public OutgoingMessage(String command, String message) {
        this.reply = new JSONObject();

        switch (command) {
            case "AUTHENTICATE":
                reply.put("command", command);
                reply.put("secret", message);
                break;
            case "ACTIVITY_BROADCAST":
                reply.put("command", command);
                reply.put("activity", message);

                break;
            case "AUTHENTICATION_FAIL":
                reply.put("command", command);
                reply.put("info", "The supplied secret was incorrect :" + message);
                break;
            case "REGISTER_SUCCESS":
                reply.put("command", command);
                reply.put("info", "You registered successfully");
                break;
            case "REGISTER_FAILED":
                reply.put("command", command);
                reply.put("info", "Username already exists");
                break;
            default:
                reply.put("command", command);
                reply.put("info", message);
                break;

        }

    }

    public OutgoingMessage() {
        this.reply = new JSONObject();
    }

    public OutgoingMessage(String command, JSONObject activity) {
        this.reply = new JSONObject();
        reply.put("command", command);
        reply.put("activity", activity);
    }

    /**
     * Used for REDIRECT,
     * For all lock related commands the keys are the same
     * LOCK_REQUEST,
     * LOCK_DENIED,
     * LOCK_ALLOWED,
     * AUTHENTICATION_FAIL
     *
     * @param command
     * @param message
     * @param info
     */
    public OutgoingMessage(String command, String message, String info) {
        this.reply = new JSONObject();

        switch (command) {

            case "REDIRECT":
                reply.put("command", command);
                reply.put("hostname", message);
                reply.put("port", info);
                break;
            case "LOGIN":
            case "REGISTER":
            case "LOCK_REQUEST":
            case "LOCK_ALLOWED":
            case "LOCK_DENIED":
                reply.put("command", command);
                reply.put("username", message);
                reply.put("secret", info);
                break;

        }
    }

    /**
     * Used for others
     *
     * @param command
     * @param secret
     * @param load
     * @param hostname
     * @param port
     */
    public OutgoingMessage(String command, String secret, String load, String hostname, String port) {
        reply = new JSONObject();
        switch (command) {

            case "SERVER_ANNOUNCE":
                reply.put("command", command);
                reply.put("id", secret);
                reply.put("load", load);
                reply.put("hostname", hostname);
                reply.put("port", port);

                break;

        }

    }

    public OutgoingMessage(String command, String username, String secret, JSONObject activity) {
        reply = new JSONObject();
        switch (command) {

            case "ACTIVITY_MESSAGE":
                reply.put("command", command);
                reply.put("username", username);
                reply.put("secret", secret);
                reply.put("activity", activity);
                break;
        }

    }


    public String getJsonReply() {
        return this.reply.toJSONString();
    }

    public JSONObject getReply() {
        return reply;
    }
}
