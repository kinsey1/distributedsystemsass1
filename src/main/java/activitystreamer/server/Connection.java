package activitystreamer.server;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import activitystreamer.util.Settings;


public class Connection extends Thread {
    private static final Logger log = LogManager.getLogger();
    private DataInputStream in;
    private DataOutputStream out;
    private BufferedReader inreader;
    private BufferedWriter outwriter;
    private PrintWriter printwriter;
    private boolean open = false;
    private Socket socket;
    private boolean term = false;
    private boolean isServer;
    private boolean authenticated;
    private Settings localSetting;

    public Connection(Socket socket) throws IOException {
        in = new DataInputStream(socket.getInputStream());
        out = new DataOutputStream(socket.getOutputStream());
        inreader = new BufferedReader(new InputStreamReader(in));
        outwriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));
        printwriter = new PrintWriter(out, true);
        this.socket = socket;
        open = true;
        start();
    }

    /*
     * returns true if the message was written, otherwise false
     */
    public boolean writeMsg(String msg) throws IOException {
        if (open) {
            outwriter.write(msg + "\n");
            outwriter.flush();
            return true;
        }
        return false;
    }

    public void closeCon() {
        if (open) {
            log.info("closing connection " + Settings.socketAddress(socket));
            try {
                term = true;
                inreader.close();
                out.close();
                //TODO Remove from connections array
            } catch (IOException e) {
                // already closed?
                log.error("received exception closing the connection " + Settings.socketAddress(socket) + ": " + e);
            }
        }
    }


    public void run() {
        try {
            String data;
            while (!term && (data = inreader.readLine()) != null) {
                log.debug("incoming msg: " + data);

                //System.out.println(Control.getInstance());
                term = Control.getInstance().process(this, data);

            }
            log.debug("connection closed to " + Settings.socketAddress(socket));
            Control.getInstance().connectionClosed(this);
            in.close();
        } catch (IOException e) {
            //log.error("connection "+Settings.socketAddress(socket)+" closed with exception: "+e);
            Control.getInstance().connectionClosed(this);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        open = false;
    }

    public void setServer(boolean isServer) {
        this.isServer = isServer;
    }

    public Socket getSocket() {
        return socket;
    }

    public boolean isOpen() {
        return open;
    }

    public boolean isServer() {
        return isServer;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticate(boolean status) {
        this.authenticated = status;
    }
}
