package activitystreamer.server;

/**
 * Class which handles registeration locks
 * This keeps track of the client, connection and
 * how many lock alloweds it has received.
 */
public class RegisterLock {
    int allowedServers;
    int totalServers;
    Client client;
    Connection con;


    public RegisterLock(Client client, int totalServers, Connection con) {
        this.client = client;
        this.totalServers = totalServers;
        this.con = con;
    }

    public void addAllowed() {
        this.allowedServers += 1;
    }

    public boolean status() {
        if (totalServers == allowedServers) {
            return true;
        }
        return false;
    }

    public Client getClient() {
        return this.client;
    }

    public Connection getCon() {
        return this.con;
    }

}
